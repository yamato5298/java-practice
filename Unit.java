
public class Unit {
  
  private
	  int rank;
      String name;
  
  public
      int getRank(){return rank;}
      String getName(){return name;}
      void setRank(int r){rank = r;}
      void setName(String s){name = s;}
      void printRank(){System.out.print(rank+"\n");}
      void printName(){System.out.print(name+"\n");}
      Unit(int r, String s){
    	  rank = r;
    	  name = s;
      }
}
