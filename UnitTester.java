import java.util.Scanner;

public class UnitTester {

	public static void main(String[] args) {
		Unit myUnit;
		Scanner rank_input, name_input;
        int rank;
        char[] FromStringRank; 
        String name, stringRank;//stringRank is parsed into an 
                                //int later
        System.out.print("Enter rank of the unit: \n");
        rank_input = new Scanner(System.in);
        stringRank = rank_input.next();
        FromStringRank = stringRank.toCharArray();//turns rank_input
                                                //into a char array
        for(int i = 0; i < stringRank.length(); i++){
        	System.out.println(FromStringRank[i]);
        	if(FromStringRank[i] != '0' && FromStringRank[i] != '1'
        			&& FromStringRank[i] != '2' && FromStringRank[i] != '3' 
        			&& FromStringRank[i] != '4' && FromStringRank[i] != '5'
        			&& FromStringRank[i] != '6'&& FromStringRank[i] != '7'
        			&& FromStringRank[i] != '8'&& FromStringRank[i] != '9'){
        		System.out.print("Error: Bad rank input. Closing game.");
        		return;
        	}//Check that rank_input does not include chars that
        	 //are not digits
        }
        //System.out.println(FromStringRank);
        rank = Integer.parseInt(stringRank);//parseInt(int) turns "int"
                                            //into int
        System.out.print("Enter name of the unit: \n");
        name_input = new Scanner(System.in);
        name = name_input.next();
        myUnit = new Unit(rank, name);
        myUnit.printName();
        myUnit.printRank();
        myUnit.setName("Dragoon");
        myUnit.printName();
        myUnit.setRank(myUnit.getRank() + 6);
        myUnit.printRank();
        return;
	}

}
